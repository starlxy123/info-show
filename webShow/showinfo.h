#ifndef SHOWINFO_H
#define SHOWINFO_H

#include <QWidget>
#include <QVector>
#include <QObject>

QT_BEGIN_NAMESPACE
namespace Ui { class ShowInfo; }
QT_END_NAMESPACE

class ShowInfo : public QWidget
{
    Q_OBJECT

public:
    ShowInfo(QWidget *parent = nullptr);
    ~ShowInfo();

private slots:
    void on_choose_currentTextChanged(const QString &arg1); // 切换网卡
    void on_init_clicked(); // 点击检测按钮

private:
    void initIpInfo();  // 获取最新ip地址
    void initMac(); // 获取mac地址
    void destroyInfo(); // 每一次点击按钮需要刷新，清除上一次记录
    void initTcpStatus();   // 获取最新tcp端口状态
    // tcp文件内容解析函数
    QString convertSeq(QString &seq);   // 格式化序号
    QString convertIp(QString &ip); // 把十六进制ip端口转换为点分十进制
    int charWeight(QChar ch);   // 辅助上函数进制转换
    QString convertLink(QString &link); // 解析tcp状态码

    Ui::ShowInfo *ui;
    struct Info
    {
        QString name;
        QString ip;
        QString mask;
        QString mac;
        Info(QString n, QString i, QString m){
            name = n; ip = i; mask = m;
        }
    };
    QVector<Info*> infoList;
};
#endif // SHOWINFO_H
