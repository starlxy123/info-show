# 基于linux的网卡信息识别工具

#### 介绍
对于win获取网络参数，都有比较方便的接口可以使用，在linux上想获取相关参数，需要自己解析相关文件，解析数据，然后可视化展示，本程序通过qt开发

[程序说明文档](https://gitee.com/starlxy123/info-show/blob/master/222021321072011%E5%88%98%E7%AC%91%E5%AE%87-%E7%BD%91%E7%BB%9C%E7%BC%96%E7%A8%8B%E6%8A%80%E6%9C%AF%E5%AE%9E%E9%AA%8C%E6%8A%A5%E5%91%8A-1.doc)